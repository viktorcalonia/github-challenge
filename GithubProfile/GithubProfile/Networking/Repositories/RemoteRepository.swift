//
//  RemoveRepository.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/14/21.
//

import Foundation

protocol RemoteRepositoryAPIProtocol {
  var endpoint: String { get }
  var parameters: [String: String]? { get }
  var method: APIClient.Method { get }
}

protocol RemoteRepositoryProtocol {
  associatedtype T
  var apiClient: APIClientProtocol { get }
}

extension RemoteRepositoryProtocol {
  func request<T>(remoteAPI: RemoteRepositoryAPIProtocol,
                  serializer: @escaping (Data) throws -> T,
                  result: @escaping (Result<T, ResponseError>) -> Void) -> Pausable {
    return apiClient.request(
      endpoint: remoteAPI.endpoint,
      method: remoteAPI.method,
      parameters: remoteAPI.parameters,
      serializer: serializer,
      result: { apiClientResult in

        switch apiClientResult {
        case .success(let model):
          result(Result<T, ResponseError>.success(model))
        case .failure(let error):
          result(Result<T, ResponseError>.failure(error))
        }
      })
  }

}

protocol ExponentialBackOffRemoteRepository: class {
  var tasks: [String: Pausable] { get set }
  var serverRelatedFailCounts: [String: Int] { get set }
  var maxCount: Int { get}
  var lastRetryDates: [String: Date] { get set }
}

extension ExponentialBackOffRemoteRepository {
  func handleServerErrorIfNeeded(for api: RemoteRepositoryAPIProtocol, error: ResponseError) {
    switch error {
    case .serverRelated:
      var serverRelatedFailCount = serverRelatedFailCounts[api.endpoint] ?? 0
      serverRelatedFailCount += 1
      serverRelatedFailCounts[api.endpoint] = serverRelatedFailCount

      scheduleRetryRequest(for: api)
    default:
      break
    }
  }

  func clearExponentialBackoffData(for api: RemoteRepositoryAPIProtocol) {
    tasks[api.endpoint] = nil
    lastRetryDates[api.endpoint] = nil
    serverRelatedFailCounts[api.endpoint] = 0
  }
}

private extension ExponentialBackOffRemoteRepository {
  func retryRequestIfNeeded(for api: RemoteRepositoryAPIProtocol) {
    if let serverRelatedFailCount = serverRelatedFailCounts[api.endpoint],
       serverRelatedFailCount < maxCount,
       let lastRetryDate = lastRetryDates[api.endpoint],
       Date().timeIntervalSince(lastRetryDate) >= exp2(Double(serverRelatedFailCount)
     ) {
      tasks[api.endpoint]?.resume()

      let lastRetryDate: Date = .init()
      lastRetryDates[api.endpoint] = lastRetryDate
    }
  }

  func scheduleRetryRequest(for api: RemoteRepositoryAPIProtocol) {
    let nextRetry = exp2(Double(serverRelatedFailCounts[api.endpoint] ?? 0))
    DispatchQueue.main.asyncAfter(deadline: .now() + nextRetry) { [weak self] in
      self?.retryRequestIfNeeded(for: api)
    }
  }
}

class BaseRemoteRepository: ExponentialBackOffRemoteRepository {
  //  using default config
  //  public static let DEFAULT_INITIAL_INTERVAL_MILLIS: Int = 500 // 0.5 seconds
  //
  //  public static let DEFAULT_MAX_ELAPSED_TIME_MILLIS: Int = 900000 // 15 minutes
  //
  //  public static let DEFAULT_MAX_INTERVAL_MILLIS: Int = 60000 // Intervall won't increase any more - 5 minutes
  //
  //  public static let DEFAULT_MULTIPLIER: Double = 1.5 // 1.5
  //
  //  public static let DEFAULT_RANDOMIZATION_FACTOR: Double = 0.5 // 0.5 or 50%

  var tasks: [String: Pausable] = [:]
  var serverRelatedFailCounts: [String: Int] = [:]
  var maxCount: Int = 6
  var lastRetryDates: [String: Date] = [:]
}

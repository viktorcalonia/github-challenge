//
//  UserListCoordinator.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/14/21.
//

import Foundation
import UIKit
import CoreData

final class UserListCoordinator: Coordinator, HasUINavigation {

  let navigation: UINavigationController
  let persistentContainer: PersistentContainer

  let apiClient: APIClientProtocol

  init(apiClient: APIClientProtocol,
       persistentContainer: PersistentContainer) {
    let userListController = UserListController(style: .plain)
    let nc = UINavigationController(rootViewController: userListController)
    navigation = nc
    self.persistentContainer = persistentContainer
    self.apiClient = apiClient
  }

  func start() {
    guard let userListController = navigation.viewControllers.first as? UserListController else { return }
    userListController.delegate = self

    let remoteRepository = GithubUserRemoteRepository(apiClient: apiClient)
    userListController.viewModel = UserListViewModel(remoteRepository: remoteRepository)
  }
}

extension UserListCoordinator: UserListControllerDelegate {
  func userListShouldOpenUserProfile(
    _ userListController: UserListController,
    viewModel: GithubUserViewModelProtocol) {
    let vc = UserProfileController()
    let remoteRepo = GithubUserProfileRemoteRepository(
      apiClient: apiClient
    )
    vc.viewModel = UserProfileSavingViewModel(
      userName: viewModel.name,
      remoteRepository: remoteRepo
    )
    vc.title = viewModel.name
    navigation.pushViewController(vc, animated: true)
  }
}

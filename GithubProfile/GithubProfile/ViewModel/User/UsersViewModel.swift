//
//  UsersViewModel.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/12/21.
//

import Foundation

protocol UsersViewModelProtocol {
  var imageUrlString: String { get }
  var name: String { get }
  var details: String { get }
}

//
//  UserProfileRemoteRepositoryTests.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/16/21.
//

import XCTest

@testable import GithubProfile

class UserProfileRemoteRepositoryTests: XCTestCase {

  let username = "indirect" // username that is available

  func testGetUserProfile() {
    let apiClient = APIClient(baseUrl: "https://api.github.com/")
    let userProfileRemoteRepo = GithubUserProfileRemoteRepository(apiClient: apiClient)

    let getUserProfile = expectation(description: "Get User Profile")
    userProfileRemoteRepo.getUserProfile(withUserName: username) { (getProfileResult) in
      switch getProfileResult {
      case .success(let profile):
        XCTAssertEqual(profile.username, self.username)
        getUserProfile.fulfill()
      default:
        break
      }
    }

    wait(for: [getUserProfile], timeout: 5)
  }

  func testSingleRunningTask() {
    let apiClient = MockAPIClient()
    let remoteRepo = GithubUserProfileRemoteRepository(apiClient: apiClient)

    remoteRepo.getUserProfile(withUserName: username) { result in
    }

    remoteRepo.getUserProfile(withUserName: username) { (result) in

    }

    XCTAssertEqual(apiClient.called, [.request])
  }
}

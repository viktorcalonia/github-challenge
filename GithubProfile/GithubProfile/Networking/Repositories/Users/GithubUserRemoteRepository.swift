//
//  GithubUserRemoteRepository.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation

typealias UsersResponseResult = (Result<[User], ResponseError>) -> Void

protocol GithubUserRemoteRepositoryProtocol {
  func getUsers( sinceUserId: Int64, result: @escaping UsersResponseResult)

  func getUsers(result: @escaping UsersResponseResult)
}

final class GithubUserRemoteRepository:
  BaseRemoteRepository, RemoteRepositoryProtocol, GithubUserRemoteRepositoryProtocol {
  typealias T = User

  let apiClient: APIClientProtocol

  init(apiClient: APIClientProtocol) {
    self.apiClient = apiClient
  }

  func getUsers(result: @escaping UsersResponseResult) {
    self.getUsers(sinceUserId: 0, result: result)
  }

  func getUsers(sinceUserId: Int64, result: @escaping UsersResponseResult) {
    let api = GithubUserRemoteAPI.getUsers(lastId: sinceUserId)

    let storedTask = tasks[api.endpoint]
    guard storedTask == nil else { return }

    let task = request(
      remoteAPI: api,
      serializer: UsersResponseSerializer.make,
      result: { [weak self] (usersResult) in
        switch usersResult {
        case .success(let users):
          self?.clearExponentialBackoffData(for: api)
          result(.success(users))
        case .failure(let error):
          if error == .serverRelated {
            self?.handleServerErrorIfNeeded(for: api, error: error)
            return
          }
          self?.clearExponentialBackoffData(for: api)
          result(.failure(error))
        }
      })
    tasks[api.endpoint] = task
  }
}

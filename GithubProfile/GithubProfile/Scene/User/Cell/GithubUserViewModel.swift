//
//  GithubUserViewModel.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/17/21.
//

import Foundation

class GithubUserViewModel: GithubUserViewModelProtocol {
  let user: User

  init(user: User) {
    self.user = user
  }
}

extension GithubUserViewModel {
  var name: String {
    user.username ?? ""
  }

  var avatarUrl: URL? {
    guard let urlString = user.avatarUrl else { return nil }
    return URL(string: urlString)
  }

  var detailText: String {
    return "Node Id: \(user.nodeId ?? "n/a")"
  }

  var note: String? {
    return ""
  }
}

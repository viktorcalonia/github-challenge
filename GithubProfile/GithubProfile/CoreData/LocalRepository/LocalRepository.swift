//
//  LocalRepository.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/17/21.
//

import Foundation

protocol LocalRepositoryProtocol {
  associatedtype T
  var persistentContainer: PersistentContainer { get }
}

extension LocalRepositoryProtocol {

}

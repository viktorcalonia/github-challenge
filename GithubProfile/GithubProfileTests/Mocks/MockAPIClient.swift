//
//  MockAPIClient.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/18/21.
//

import Foundation

@testable import GithubProfile

final class MockAPIClient: APIClientProtocol {

  enum Method: Equatable {
    case request
  }

  var called: [Method] = []

  func request<T>(
    endpoint: String,
    method: APIClient.Method,
    parameters: [String : Any]?,
    serializer: @escaping (Data) throws -> T,
    result: @escaping (Result<T, ResponseError>) -> Void) -> Pausable {
    called.append(.request)
    return MockPausable()
  }
}


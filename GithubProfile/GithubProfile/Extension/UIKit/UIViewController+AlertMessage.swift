//
//  UIViewController.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/17/21.
//

import Foundation
import UIKit

enum AlertType: String {
  case info
  case error
  case normal

  var title: String? {
    switch self {
    case .normal:
      return nil
    default:
      return rawValue.capitalized
    }
  }
}

extension UIViewController {
  func showErrorAlert(with message: String, type: AlertType) {
    let alert = UIAlertController(title: type.title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
    present(alert, animated: true, completion: nil)
  }
}

//
//  GithubUserViewModelProtocol.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation

protocol GithubUserViewModelProtocol {
  var name: String { get }
  var avatarUrl: URL? { get }
  var detailText: String { get }
  var note: String? { get }
}

//
//  ResponseError.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation

enum ResponseError: Error, Equatable {
  case conflict
  case couldNotParse
  case invalidURL
  case networkStatus(Code)
  case noResponse
  case serverRelated
  case unknown(Int)

  public enum Code: Int {
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
    case notFound = 404
    case noInternet = 900
  }

  public var description: String {
    switch self {
    case .networkStatus(let code):
      return String("Network response: \(code.rawValue)")
    case .couldNotParse:
      return "Could not parse"
    case .noResponse:
      return "No response"
    case .invalidURL:
      return "Invalid URL"
    case .conflict:
      return "Conflict"
    case .serverRelated:
      return "Server Related Error"
    case .unknown(let code):
      return "Unknown error: \(code)"
    }
  }
}

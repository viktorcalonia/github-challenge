//
//  ImageCache.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/21/21.
//

import Foundation
import UIKit

final class ImageCache {
  static let shared = ImageCache()

  enum CacheLevel: Equatable {
    case all
    case memoryOnly
    case diskOnly
  }

  let imageCache = NSCache<NSString, UIImage>()

  func saveImage(_ image: UIImage, with key: String, cacheLevel: CacheLevel = .all) {
    if cacheLevel == .all || cacheLevel == .memoryOnly {
      saveImageInMemory(withImage: image, identifier: key)
    }

    if cacheLevel == .all || cacheLevel == .diskOnly {
      saveToDiskCache(withImage: image, identifier: key)
    }
  }

  func image(with key: String) -> UIImage? {
    if let memCacheImage = imageCache.object(forKey: NSString(string: key)) {
      return memCacheImage
    }

    do {
      if let diskCacheImage = try imageFromDiskCache(with: key) {
        return diskCacheImage
      }
    } catch let error {
      print("#function")
      print(error.localizedDescription)
    }

    return nil
  }

  private func saveImageInMemory(withImage image: UIImage, identifier: String) {
    imageCache.setObject(image, forKey: NSString(string: identifier))
  }

  private func saveToDiskCache(withImage image: UIImage, identifier: String) {
    let fileManager = FileManager.default
    guard let data = image.jpegData(compressionQuality: 1)
            ?? image.pngData()
    else { return print("\(#function) Error in generating image data") }

    guard let directory = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
    else { return  print("\(#function): Error Finding Directory") }

    let fileName = "\(identifier).png"
    let fileUrl = directory.appendingPathComponent(fileName)

    do {
      if fileManager.fileExists(atPath: fileUrl.path) {
        try fileManager.removeItem(atPath: fileUrl.path)
      }
      try data.write(to: fileUrl)
    } catch {
      print("\(#function): \(error.localizedDescription)")
    }
  }

  private func imageFromDiskCache(with identifier: String) throws -> UIImage? {
    let fileManager = FileManager.default
    guard let directory = fileManager.urls(
            for: .documentDirectory,
            in: .userDomainMask) .first
    else {
      print("\(#function): Error Finding Directory")
      return nil
    }

    let fileName = "\(identifier).png"
    return UIImage(
      contentsOfFile: URL(
        fileURLWithPath: directory.absoluteString)
        .appendingPathComponent(fileName).path
    )
  }
}

//
//  UserProfileViewModel.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/20/21.
//

import Foundation

typealias VoidResult = (Result<Void, Error>) -> Void
typealias ProfileModelResult = (Result<UserProfileViewModelProtocol, Error>) -> Void

protocol UserProfileViewModelProtocol {
  var avatarUrl: URL? { get }

  var name: String { get }
  var blog: String { get }
  var location: String { get }
  var email: String { get }
  var company: String { get }
  var bio: String { get }
  var publicReposText: String { get }
  var publicGistsText: String { get }

  var followingsText: String { get }
  var followeraText: String { get }

  var notes: String { get }
}

struct UserProfileViewModel {

  let userProfile: UserProfile

  private let notAvailable = "n/a"

  init(userProfile: UserProfile) {
    self.userProfile = userProfile
  }
}

extension UserProfileViewModel: UserProfileViewModelProtocol {

  var avatarUrl: URL? {
    URL(string: userProfile.avatarUrl ?? "")
  }

  var name: String {
    userProfile.name ?? notAvailable
  }

  var blog: String {
    userProfile.blog ?? notAvailable
  }

  var location: String {
    userProfile.location ?? notAvailable
  }

  var email: String {
    userProfile.email ?? notAvailable
  }

  var company: String {
    userProfile.company ?? notAvailable
  }

  var bio: String {
    userProfile.bio ?? notAvailable
  }

  var publicReposText: String {
    String(userProfile.publicRepos)
  }

  var publicGistsText: String {
    String(userProfile.publicGists)
  }

  var followingsText: String {
    "Followings: \(userProfile.followingCount)"
  }

  var followeraText: String {
    "Followers: \(userProfile.followerCount)"
  }

  var notes: String {
    userProfile.notes ?? ""
  }
}

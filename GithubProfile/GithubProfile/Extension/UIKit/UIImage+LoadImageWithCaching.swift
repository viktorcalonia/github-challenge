//
//  UIImage+LoadImageWithCaching.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/17/21.
//

import Foundation
import UIKit

typealias ImageResultBlock = (Result<UIImage, Error>) -> Void

private let imageCache = NSCache<NSString, UIImage>()
private let apiClient = APIClient(baseUrl: "")

enum ImageLoadingError: Error, Equatable {
  case cacheDirectoryAccess
  case savingToDiskCache
  case imageNotFound
}

extension UIImageView {
  func setImage(url: URL, result: ImageResultBlock? = nil) {
    setImage(urlString: url.absoluteString, result: result)
  }

  func setImage(urlString: String, result: ImageResultBlock? = nil) {
    func setImage(with image: UIImage) {
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        self.image = image
        result?(.success(image))
      }
    }

    if let image = ImageCache.shared.image(with: urlString) {
      setImage(with: image)
      return
    }

    guard let url = URL(string: urlString) else { return }
    apiClient.getImage(url: url) { (imageResult) in
      switch imageResult {
      case .success(let image):
        setImage(with: image)
        ImageCache.shared.saveImage(image, with: urlString)
      case .failure(let error):
        result?(.failure(error))
      }
    }
  }

  private func saveImageInMemory(withImage image: UIImage, urlString: String) {
    imageCache.setObject(image, forKey: urlString as NSString)
  }

  private func saveToDiskCache(withImage image: UIImage, urlString: String) {
    do {
      guard let directoryPath = try getCacheDirectoryPath() else { return }
      try image.pngData()?.write(to: directoryPath.appendingPathComponent(urlString))
    } catch let error {
      print(#function)
      print(error.localizedDescription)
    }
  }

  private func imageFromDiskCache(with urlString: String) throws -> UIImage? {
    guard let directoryPath = try getCacheDirectoryPath() else { return nil }
    return UIImage(contentsOfFile: directoryPath.appendingPathComponent(urlString).absoluteString)
  }

  private func getCacheDirectoryPath() throws -> URL?  {
    do {
      return try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    } catch let error {
      print(#function)
      print(error.localizedDescription)
      throw ImageLoadingError.cacheDirectoryAccess
    }
  }
}



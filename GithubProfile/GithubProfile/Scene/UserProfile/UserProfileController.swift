//
//  UserProfileController.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/20/21.
//

import Foundation
import UIKit

class UserProfileController: UIViewController {

  var viewModel: UserProfileSavingProtocol?

  @IBOutlet var avatarImage: UIImageView!

  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var blogLabel: UILabel!
  @IBOutlet var location: UILabel!
  @IBOutlet var email: UILabel!
  @IBOutlet var company: UILabel!
  @IBOutlet var bio: UILabel!
  @IBOutlet var publicRepos: UILabel!
  @IBOutlet var publicGists: UILabel!

  @IBOutlet var followerLabel: UILabel!
  @IBOutlet var followingLabel: UILabel!

  @IBOutlet var noteTextView: UITextView!
  @IBOutlet var noteHeightLayoutConstraints: NSLayoutConstraint!

  @IBOutlet var infoContainerView: UIView!
  @IBOutlet var noteContainerView: UIView!

  override func viewDidLoad() {
    super.viewDidLoad()
    loadData()
    setupUI()
    setupNavigation()
  }

  private func loadData() {
    if let userProfile = viewModel?.userProfileModel {
      setupViews(with: userProfile)
    } else {
      fetchViewInfo()
    }
  }

  private func setupUI() {
    infoContainerView.applyBorder()
    noteContainerView.applyBorder()
  }

  private func setupNavigation() {
    navigationItem.backButtonTitle = ""
    navigationItem.backBarButtonItem?.title = ""
    navigationController?.navigationBar.tintColor = .black
  }

  private func fetchViewInfo() {
    viewModel?.fetchProfile(result: { [weak self] (userProfileVMResult) in
      guard let self = self else { return }
      switch userProfileVMResult {
      case .success(let profilevm):
        self.setupViews(with: profilevm)
      case .failure(let error):
        print(#function)
        print(error.localizedDescription)
      }
    })
  }

  private func setupViews(with userProfileVM: UserProfileViewModelProtocol) {
    if let url = userProfileVM.avatarUrl {
      avatarImage.setImage(url: url)
    }

    nameLabel.text = userProfileVM.name
    blogLabel.text = userProfileVM.blog
    email.text = userProfileVM.email
    company.text = userProfileVM.company

    location.text = userProfileVM.location
    bio.text = userProfileVM.bio

    publicRepos.text = userProfileVM.publicReposText
    publicGists.text = userProfileVM.publicGistsText

    followerLabel.text = userProfileVM.followeraText
    followingLabel.text = userProfileVM.followingsText

    noteTextView.text = userProfileVM.notes
  }
}

extension UserProfileController: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    let size = CGSize(width: textView.frame.width, height: .infinity)
    let estimatedSize = textView.sizeThatFits(size)

    noteHeightLayoutConstraints.constant = max(estimatedSize.height, 150)
    view.layoutIfNeeded()
  }
}

extension UserProfileController {
  @IBAction func didTapSave() {
    viewModel?.saveNote(noteTextView.text, result: { [weak self] (userProfileResult) in
      guard let self = self else { return }
      switch userProfileResult {
      case .success(let userProfile):
        self.setupViews(with: userProfile)
      case .failure(let error):
        print(error.localizedDescription)
      }
    })
  }
}

//
//  UserResponseSerializer.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation

enum UsersResponseSerializer: SerializerProtocol {
  typealias ResponseModel = [User]

  static func make(_ data: Data) throws -> [User] {
    return try transform(data: data)
  }
}

//
//  SerializerProtocol.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation
protocol SerializerProtocol {
  associatedtype ResponseModel: Codable
}

extension SerializerProtocol {
  static func transform(data: Data) throws -> ResponseModel {
    let decoder = makeJSONDecoder()
    return try decoder.decode(ResponseModel.self, from: data)
  }

  static func transform(model: ResponseModel) throws -> Data {
    let encoder = makeJSONEncoder()
    return try encoder.encode(model)
  }

  private static func makeJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    decoder.dateDecodingStrategy = .formatted(.iso8601)
    return decoder
  }

  private static func makeJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    encoder.keyEncodingStrategy = .convertToSnakeCase
    encoder.dateEncodingStrategy = .formatted(.iso8601)
    return encoder
  }
}

private extension DateFormatter {
  static let iso8601: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
    formatter.calendar = Calendar(identifier: .iso8601)
    return formatter
  }()
}

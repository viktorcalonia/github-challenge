//
//  ImageCacheTest.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/21/21.
//

import XCTest

@testable import GithubProfile

class ImageCacheTest: XCTestCase {

  let imageCache = ImageCache()
  func testImageCaching() {
    let name = "pencil"
    let image = UIImage(systemName: name) ?? UIImage()
    imageCache.saveImage(image, with: name)

    let savedImage = imageCache.image(with: name)
    XCTAssertNotNil(savedImage)
  }

  func testMemoryImageCaching() {
    let name = "pencil"
    let image = UIImage(systemName: name) ?? UIImage()
    imageCache.saveImage(image, with: name, cacheLevel: .memoryOnly)

    let savedImage = imageCache.image(with: name)
    XCTAssertNotNil(savedImage)
  }

  func testDiskImageCaching() {
    let name = "pencil"
    let image = UIImage(systemName: name) ?? UIImage()
    imageCache.saveImage(image, with: name, cacheLevel: .diskOnly)

    let savedImage = imageCache.image(with: name)
    XCTAssertNotNil(savedImage)
  }
}

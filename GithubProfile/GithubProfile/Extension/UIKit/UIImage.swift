//
//  UIImage.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

extension UIImage {
  var invertedImage: UIImage? {
    let beginImage = CIImage(image: self)
    if let filter = CIFilter(name: "CIColorInvert") {
      filter.setValue(beginImage, forKey: kCIInputImageKey)
      return UIImage(ciImage: filter.outputImage!)
    }
    return nil
  }
}

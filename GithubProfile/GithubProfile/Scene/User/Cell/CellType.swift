//
//  CellType.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

enum CellType: String, CaseIterable {
  case normal
  case note
  case inverted

  var cellReuseIdentifier: String {
    return rawValue
  }

  var nib: UINib {
    switch self {
    case .inverted, .note:
      return UINib(nibName: String(describing: GithubUserNoteCell.self), bundle: nil)
    case .normal:
      return UINib(nibName: String(describing: GithubUserCell.self), bundle: nil)
    }
  }
}

//
//  GithubUserRemoteRespositoryTest.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import XCTest

@testable import GithubProfile

class UserRemoteRepositoryTest: XCTestCase {

  func testGetUsers() {
    let apiClient = APIClient(baseUrl: "https://api.github.com/")
    let remoteRepo = GithubUserRemoteRepository(apiClient: apiClient)

    let getUsers = expectation(description: "Get Users")
    remoteRepo.getUsers { (result) in
      switch result {
      case .success(let users):
        XCTAssertTrue(users.count > 0)
        getUsers.fulfill()
      default:
        break
      }
    }
    wait(for: [getUsers], timeout: 5)
  }

  func testGetUsersSingleRunningTask() {
    let apiClient = MockAPIClient()
    let remoteRepo = GithubUserRemoteRepository(apiClient: apiClient)

    remoteRepo.getUsers { (result) in
    }

    remoteRepo.getUsers { (result) in

    }

    XCTAssertEqual(apiClient.called, [.request])
  }
}

//
//  GithubUserProtocol.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

protocol GithubUserCellProtocol where Self: UITableViewCell {
  var viewModel: GithubUserViewModelProtocol! { get set }
}

//
//  UserListController.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/12/21.
//

import Foundation
import UIKit

protocol UserListControllerDelegate: class {
  func userListShouldOpenUserProfile(
    _ userListController: UserListController,
    viewModel: GithubUserViewModelProtocol
  )
}

class UserListController: UITableViewController {
  weak var delegate: UserListControllerDelegate?

  var viewModel: UserListViewModelProtocol!

  override func viewDidLoad() {
    super.viewDidLoad()

    title = ""
    setupSearchBar()
    setupTable()
    setupPullToRefresh()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    if viewModel.vmState == .initial {
      refreshTable()
    }
  }
}

private extension UserListController {
  func setupTable() {
    for type in CellType.allCases {
      tableView.register(type.nib, forCellReuseIdentifier: type.cellReuseIdentifier)
    }
    tableView.rowHeight = 80
  }

  func setupSearchBar() {
    let searchController = UISearchController(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false

    navigationItem.searchController = searchController
  }

  func setupTableFooter() {
    tableView.tableFooterView = viewModel.isSearchMode
      ? nil
      :  LoadMoreTableFooter(width: view.frame.width)
  }
}

extension UserListController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchText = searchController.searchBar.text ?? ""
    viewModel.searchItems(with: searchText) { [weak self] (searchItemResult) in
      self?.tableView.reloadData()
    }

    setupTableFooter()
  }
}

extension UserListController {
  func setupPullToRefresh() {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
    tableView.addSubview(refreshControl)
    self.refreshControl = refreshControl
  }

  @objc
  func refreshTable() {
    refreshControl?.beginRefreshing()
    viewModel.getItems { [weak self] (itemResult) in
      guard let self = self else { return }
      switch itemResult {
      case .success(_):
        self.tableView.reloadData()
        self.setupTableFooter()
      case .failure(let error):
        self.showErrorAlert(with: error.localizedDescription, type: .error)
      }
      self.refreshControl?.endRefreshing()
    }
  }

  func refreshMore() {
    viewModel.getMoreItems { [weak self] (itemsResult) in
      guard let self = self else { return }
      switch itemsResult {
      case .success(let items):
        self.appendItemToTable(with: items)
      case .failure(let error):
        self.showErrorAlert(with: error.localizedDescription, type: .error)
      }
    }
  }

  func appendItemToTable(with items: [GithubUserViewModelProtocol]) {
    let startIndex = viewModel.items.count - items.count

    let range = startIndex..<viewModel.items.count
    let indexPaths = range.compactMap({ IndexPath(row: $0, section: 0)})

    tableView.beginUpdates()
    tableView.insertRows(at: indexPaths, with: .automatic)
    tableView.endUpdates()
  }
}

extension UserListController {

  private func cellType(for indexPath: IndexPath) -> CellType {
    if indexPath.row % 4 == 0 {
      return .inverted
    }

    return .normal
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return getItemsViewModelContext().count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let type = cellType(for: indexPath)
    return tableView.dequeueReusableCell(withIdentifier: type.cellReuseIdentifier, for: indexPath)
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    delegate?.userListShouldOpenUserProfile(
      self,
      viewModel: getItemsViewModelContext()[indexPath.row]
    )
  }

  override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if let cell = cell as? GithubUserCellProtocol {
      cell.viewModel = getItemsViewModelContext()[indexPath.row]
    }
  }

  override func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.y >= (scrollView.contentSize.height - tableView.frame.height - 80),
       viewModel.vmState == .idle,
       viewModel.isSearchMode == false,
       refreshControl?.isRefreshing == false {
      refreshMore()
    }
  }

  private func getItemsViewModelContext() -> [GithubUserViewModelProtocol] {
    return viewModel.isSearchMode ? viewModel.searchedItems : viewModel.items
  }
}

//
//  UserProfileSerializer.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation

enum UserProfileSerializer: SerializerProtocol {
  typealias ResponseModel = UserProfile

  static func make(_ data: Data) throws -> UserProfile {
    return try transform(data: data)
  }
}

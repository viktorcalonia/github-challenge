//
//  UserResponse.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/12/21.
//

import Foundation

struct User: Codable {
  enum CodingKeys: String, CodingKey {
    case id
    case username = "login"
    case avatarUrl
    case nodeId
    case siteAdmin
  }

  var id: Int64
  var username: String?
  var avatarUrl: String?
  var nodeId: String?
  var siteAdmin: Bool
}

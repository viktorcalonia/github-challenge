//
//  LoadMoreTableFooter.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/17/21.
//

import Foundation
import UIKit

class LoadMoreTableFooter: UIView {

  init(width: CGFloat) {
    super.init(frame: CGRect(x: 0, y: 0, width: width, height: 80))

    setupIndicator()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }

  func setupIndicator() {
    let indicator = UIActivityIndicatorView(style: .medium)
    addSubview(indicator)
    indicator.startAnimating()

    indicator.center = center

    layoutIfNeeded()
  }
}

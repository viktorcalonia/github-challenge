//
//  UserListViewModel.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/14/21.
//

import Foundation

enum UserListViewModelState: Equatable {
  case initial
  case idle
  case loadingInitial
  case loadingMore
}

protocol UserListViewModelProtocol {
  var isSearchMode: Bool { get }
  var vmState: UserListViewModelState { get }

  var searchedItems: [GithubUserViewModelProtocol] { get }
  var items: [GithubUserViewModelProtocol] { get }

  func searchItems(
    with searchText: String,
    result: @escaping (Result<[GithubUserViewModelProtocol], Error>) -> Void
  )
  func getItems(result: @escaping (Result<[GithubUserViewModelProtocol], Error>) -> Void)
  func getMoreItems(result: @escaping (Result<[GithubUserViewModelProtocol], Error>) -> Void)
}

class UserListViewModel: UserListViewModelProtocol {

  var isSearchMode: Bool {
    return searchText.isEmpty == false
  }

  private var searchText: String = ""

  var vmState: UserListViewModelState = .initial
  var searchedItems: [GithubUserViewModelProtocol] = []
  var items: [GithubUserViewModelProtocol] = []

  private var lastItemId: Int64 = 0

  private let userRemoteRepository: GithubUserRemoteRepositoryProtocol

  init(remoteRepository: GithubUserRemoteRepositoryProtocol) {
    self.userRemoteRepository = remoteRepository
  }

  func searchItems(
    with searchedText: String,
    result: (Result<[GithubUserViewModelProtocol], Error>) -> Void) {
    self.searchText = searchedText
    searchedItems = items.filter({
      $0.name.localizedCaseInsensitiveContains(self.searchText) ||
      $0.note?.localizedCaseInsensitiveContains(self.searchText) ?? false
    })
    result(.success(searchedItems))
  }

  func getItems(result: @escaping (Result<[GithubUserViewModelProtocol], Error>) -> Void) {
    vmState = .loadingInitial
    userRemoteRepository.getUsers { [weak self] (usersResult) in
      guard let self = self else { return }
      self.vmState = .idle
      switch usersResult {
      case .success(let users):
        self.lastItemId = users.last?.id ?? 0
        let items = users.map({ self.makeGithubUserViewModel(with: $0 )})
        self.items = items
        DispatchQueue.main.async {
          result(.success(items))
        }
      case .failure(let error):
        DispatchQueue.main.async {
          result(.failure(error))
        }
      }
    }
  }

  func getMoreItems(result: @escaping (Result<[GithubUserViewModelProtocol], Error>) -> Void) {
    guard lastItemId != 0 else { return }
    vmState = .loadingMore
    userRemoteRepository.getUsers(sinceUserId: lastItemId) { [weak self] (usersResult) in
      guard let self = self else { return }
      self.vmState = .idle
      switch usersResult {
      case .success(let users):
        self.lastItemId = users.last?.id ?? 0
        let items = users.map({ self.makeGithubUserViewModel(with: $0) })
        self.items.append(contentsOf: items)
        DispatchQueue.main.async {
          result(.success(items))
        }
      case .failure(let error):
        DispatchQueue.main.async {
          result(.failure(error))
        }
      }
    }
  }
}

private extension UserListViewModel {
  func makeGithubUserViewModel(with user: User) -> GithubUserViewModelProtocol {
    return GithubUserViewModel(user: user)
  }
}



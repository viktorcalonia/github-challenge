//
//  UserProfileResponse.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation

struct UserProfile: Codable {
  enum CodingKeys: String, CodingKey {
    case id
    case username = "login"
    case avatarUrl
    case name
    case blog
    case company
    case location
    case email
    case bio
    case publicRepos
    case publicGists
    case followerCount = "followers"
    case followingCount = "following"
  }

  var id: Int64
  var username: String?
  var avatarUrl: String?

  var name: String?
  var blog: String?
  var company: String?
  var location: String?
  var email: String?
  var bio: String?
  var publicRepos: Int64
  var publicGists: Int64
  var followerCount: Int64
  var followingCount: Int64

  var notes: String?
}

//
//  UserProfileSavingViewModel.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/21/21.
//

import Foundation

protocol UserProfileSavingProtocol {
  var userProfileModel: UserProfileViewModelProtocol? { get }

  func saveNote(_ note: String, result: @escaping ProfileModelResult)
  func fetchProfile(result: @escaping ProfileModelResult)
}

final class UserProfileSavingViewModel: UserProfileSavingProtocol {
  var userProfileModel: UserProfileViewModelProtocol?

  let remoteRepository: GithubUserProfileRemoteRepositoryProtocol
  let userName: String

  init(userName: String, remoteRepository: GithubUserProfileRemoteRepositoryProtocol) {
    self.userName = userName
    self.remoteRepository = remoteRepository
  }

  func fetchProfile(result: @escaping ProfileModelResult) {
    remoteRepository.getUserProfile(withUserName: userName) { [weak self] (userProfileResult) in
      guard let self = self else { return }
      switch userProfileResult {
      case .success(let userProfile):
        let profileVM = UserProfileViewModel(userProfile: userProfile)
        self.userProfileModel = profileVM
        DispatchQueue.main.async {
          result(.success(profileVM))
        }
      case .failure(let error):
        DispatchQueue.main.async {
          result(.failure(error))
        }
      }
    }
  }

  func saveNote(_ note: String, result: @escaping ProfileModelResult) {
    guard let userProfile = userProfileModel else { return }
    result(.success(userProfile))
  }
}

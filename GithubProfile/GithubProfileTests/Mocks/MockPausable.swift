//
//  MockPausable.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/18/21.
//

import Foundation

@testable import GithubProfile

final class MockPausable: Pausable {
  enum Method: Equatable {
    case cancel
    case suspend
    case resume
  }

  var called: [Method] = []

  func cancel() {
    called.append(.cancel)
  }

  func suspend() {
    called.append(.suspend)
  }

  func resume() {
    called.append(.resume)
  }
}


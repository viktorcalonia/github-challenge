//
//  UserAvatarTest.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/19/21.
//

import XCTest

@testable import GithubProfile

class UserAvatarTest: XCTestCase {

  func testGetAvatar() {
    let apiClient = APIClient(baseUrl: "")

    let url = URL(string: "https://avatars.githubusercontent.com/u/47?v=4")!
    XCTAssertNotNil(url)

    let getImage = expectation(description: "Loading Image")
    apiClient.getImage(url: url) { (imageResult) in
      switch imageResult {
      case .success(let image):
        getImage.fulfill()
        XCTAssertNotNil(image)
      case .failure(let error):
        XCTAssertNil(error)
      }
    }
    waitForExpectations(timeout: 5, handler: nil)
  }
}

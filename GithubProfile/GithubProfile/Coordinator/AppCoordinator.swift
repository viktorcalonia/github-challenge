//
//  AppCoordinator.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/12/21.
//

import Foundation
import UIKit
import CoreData

final class AppCoordinator: ParentCoordinator {

  var windowHandler: WindowHandler
  let persistentContainer: PersistentContainer
  let apiClient: APIClientProtocol

  var children: [Coordinator] = []

  init(windowHandler: WindowHandler,
       persistentContainer: PersistentContainer,
       apiClient: APIClientProtocol) {
    self.windowHandler = windowHandler
    self.persistentContainer = persistentContainer
    self.apiClient = apiClient
  }

  func start() {
    setupRoot()
  }
}

private extension AppCoordinator {
  func setupRoot() {
    let window = windowHandler.window
    let userRoot = UserListCoordinator(
      apiClient: apiClient,
      persistentContainer: persistentContainer
    )
    window?.rootViewController = userRoot.navigation
    startChild(userRoot)
    window?.makeKeyAndVisible()
  }
}

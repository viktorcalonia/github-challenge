//
//  Coordinator.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/12/21.
//

import Foundation
import UIKit

protocol Coordinator: class {
    func start()
}

protocol CoordinatorDelegate: class {
    func didFinish(_ coordinator: Coordinator)
}

protocol ParentCoordinator: Coordinator, CoordinatorDelegate {
    var children: [Coordinator] { get set }
}

protocol HasUINavigation {
  var navigation: UINavigationController { get }
}

extension ParentCoordinator {
    func startChild(_ coordinator: Coordinator) {
        addChild(coordinator)
        coordinator.start()
    }

    func addChild(_ child: Coordinator) {
        children.append(child)
    }

    func removeChild(_ child: Coordinator) {
        children.removeAll { $0 === child }
    }

    func didFinish(_ coordinator: Coordinator) {
        removeChild(coordinator)
    }
}

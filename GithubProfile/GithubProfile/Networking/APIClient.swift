//
//  APIClient.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

typealias ImageRequestResult = (Result<UIImage, ResponseError>) -> Void

protocol APIClientProtocol {
  @discardableResult
  func request<T>(endpoint: String,
                  method: APIClient.Method,
                  parameters: [String: Any]?,
                  serializer: @escaping (Data) throws -> T,
                  result: @escaping (Result<T, ResponseError>) -> Void) -> Pausable
}

final class APIClient: APIClientProtocol {

  enum Method: String {
    case get
    case post
    case put
  }

  let session: URLSession
  let baseUrl: String

  init(baseUrl: String) {
    session = URLSession.shared
    self.baseUrl = baseUrl
  }

  @discardableResult
  func getImage(url: URL, result: @escaping ImageRequestResult) ->  Pausable{

    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
      if let response = response as? HTTPURLResponse {
        if let error = error {
          result(.failure(ResponseMapper.handleResponseError(response: response, error: error)))
        } else if let data = data {
          result(.success(UIImage(data: data)!))
        } else {
          result(.failure(ResponseError.noResponse))
        }
      }
    }
    task.resume()
    return task
  }

  @discardableResult
  func request<T>(endpoint: String,
                  method: APIClient.Method,
                  parameters: [String: Any]?,
                  serializer: @escaping (Data) throws -> T,
                  result: @escaping (Result<T, ResponseError>) -> Void) -> Pausable{
    let headers = [
      "Content-Type": "application/json",
    ]


    let request = NSMutableURLRequest(url: NSURL(string: baseUrl + endpoint)! as URL,
                                            cachePolicy: .useProtocolCachePolicy,
                                        timeoutInterval: 10.0)
    request.httpMethod = method.rawValue.uppercased()
    request.allHTTPHeaderFields = headers

    if let parameters = parameters {
      let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
      request.httpBody = postData as Data
    }

    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
      if let response = response as? HTTPURLResponse {
        if let error = error {
          result(.failure(ResponseMapper.handleResponseError(response: response, error: error)))
        } else if let data = data {
          result(ResponseMapper.resultFrom(response: response, data: data, serializer: serializer))
        } else {
          result(.failure(ResponseError.couldNotParse))
        }
      } else {
        result(.failure(ResponseError.noResponse))
      }
    })
    dataTask.resume()
    return dataTask
  }
}

protocol Pausable: class {
  func resume()
  func suspend()
  func cancel()
}

extension URLSessionDataTask: Pausable {}

class ResponseMapper {
  static func resultFrom<T>(response: HTTPURLResponse, data: Data, serializer: ((Data) throws -> T)) -> Result<T, ResponseError> {
      do {
          let resultValue = try serializer(data)
          return Result<T, ResponseError>.success(resultValue)
      } catch let error {
          debugPrint(error)
          let parseError = ResponseError.couldNotParse
          return Result<T, ResponseError>.failure(parseError)
      }
  }

  static func handleResponseError(response: HTTPURLResponse, error: Error) -> ResponseError {
    let statusCode = response.statusCode
    if let code = ResponseError.Code(rawValue: statusCode) {
      return ResponseError.networkStatus(code)
    }
    if statusCode >= 500 || statusCode < 600 {
      return ResponseError.serverRelated
    }
    return ResponseError.unknown(statusCode)
  }
}


//
//  MockResult.swift
//  GithubProfileTests
//
//  Created by Viktor Immanuel Calonia on 4/18/21.
//

import Foundation

@testable import GithubProfile

final class MockResult<Model> {
  var result: Result<Model, ResponseError>?

  func success(with value: Model) {
    result = Result<Model, ResponseError>.success(value)
  }

  func failure(with error: ResponseError) {
    result = Result<Model, ResponseError>.failure(error)
  }

  func attempt(_ completion: (Result<Model, ResponseError>) -> Void) {
    result.map(completion)
  }
}

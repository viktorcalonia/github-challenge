//
//  UIView+Extension.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

extension UIView {
  func applyShadow() {
    layer.shadowColor = UIColor.lightGray.cgColor
    layer.shadowOpacity = 1
    layer.shadowRadius = 2
    layer.shadowOffset = CGSize(width: 0, height: 0)
  }

  func applyBorder() {
    layer.borderColor = UIColor.lightGray.cgColor
    layer.borderWidth = 1
  }
}

//
//  GithubUserCell.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

class GithubUserCell: UITableViewCell, GithubUserCellProtocol {
  var viewModel: GithubUserViewModelProtocol! {
    didSet {
      setupView()
    }
  }

  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var detailLabel: UILabel!
  @IBOutlet var avatarImageView: UIImageView!

  @IBOutlet var container: UIView!

  override func awakeFromNib() {
    super.awakeFromNib()
    container.layer.cornerRadius = 10
    container.applyShadow()
  }

  func setupView() {
    nameLabel.text = viewModel.name
    detailLabel.text = viewModel.detailText
    if let url = viewModel.avatarUrl {
      avatarImageView.setImage(url: url)
    }
  }
}

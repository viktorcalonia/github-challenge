//
//  GithubRemoteAPI.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation

enum GithubUserRemoteAPI: RemoteRepositoryAPIProtocol {
  case getUsers(lastId: Int64)
  case getUserProfile(userName: String)
}

extension  GithubUserRemoteAPI {
  var endpoint: String {
    switch self {
    case .getUsers(let lastId):
      return "users?since=\(lastId)"
    case .getUserProfile(let userName):
      return "users/\(userName)"
    }
  }

  var parameters: [String : String]? {
    return nil
  }

  var method: APIClient.Method {
    return .get
  }
}

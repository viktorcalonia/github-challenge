//
//  GithubUserProfileRemoteRepository.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/15/21.
//

import Foundation

typealias  UserProfileResult = (Result<UserProfile, ResponseError>) -> Void

protocol GithubUserProfileRemoteRepositoryProtocol {
  func getUserProfile(
    withUserName userName: String,
    result: @escaping UserProfileResult
  )
}

final class GithubUserProfileRemoteRepository:
  BaseRemoteRepository, RemoteRepositoryProtocol, GithubUserProfileRemoteRepositoryProtocol {
  typealias T = UserProfile

  let apiClient: APIClientProtocol

  init(apiClient: APIClientProtocol) {
    self.apiClient = apiClient
  }

  func getUserProfile(
    withUserName userName: String,
    result: @escaping UserProfileResult) {

    let api = GithubUserRemoteAPI.getUserProfile( userName: userName)

    let storedTask = tasks[api.endpoint]
    guard storedTask == nil else { return }

    let task = request(
      remoteAPI: api,
      serializer: UserProfileSerializer.make) { [weak self] (userProfileResult) in
      switch userProfileResult {
      case .success(let userProfile):
        self?.clearExponentialBackoffData(for: api)
        result(.success(userProfile))
      case .failure(let error):
        if error == .serverRelated {
          self?.handleServerErrorIfNeeded(for: api, error: error)
          return
        }
        self?.clearExponentialBackoffData(for: api)
        result(.failure(error))
    }}
    tasks[api.endpoint] = task
  }
}

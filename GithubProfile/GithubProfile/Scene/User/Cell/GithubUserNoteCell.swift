//
//  GithubUserNoteCell.swift
//  GithubProfile
//
//  Created by Viktor Immanuel Calonia on 4/13/21.
//

import Foundation
import UIKit

class GithubUserNoteCell: UITableViewCell, GithubUserCellProtocol {
  var viewModel: GithubUserViewModelProtocol! {
    didSet {
      setupView()
    }
  }

  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var detailLabel: UILabel!
  @IBOutlet var avatarImage: UIImageView!
  @IBOutlet var noteImage: UIImageView!

  @IBOutlet var container: UIView!

  override func awakeFromNib() {
    super.awakeFromNib()
    container.layer.cornerRadius = 10
    container.applyShadow()
  }

  func setupView() {
    nameLabel.text = viewModel.name
    detailLabel.text = viewModel.detailText
    if let url = viewModel.avatarUrl {
      avatarImage.setImage(url: url)
    }
  }
}
